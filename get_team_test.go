package main

import (
	"bytes"
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/gorilla/mux"
)

func TestGetTeamWithPlayers(t *testing.T) {
	r := mux.NewRouter()
	r.HandleFunc("/soccers/players/teams/{team_name}", GetTeamWithPlayers)

	req, err := http.NewRequest("GET", "/soccers/players/teams/Arsenal", nil)
	rr := httptest.NewRecorder()
	if err != nil {
		t.Fatal(err)
	}

	r.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
}

func TestCreateTeamWithPlayers(t *testing.T) {
	var jsonStr = []byte(`{"team":{"name": "Persipura macan 2","city": "Bandung"},"player_names": ["Arip","Mamang"]}`)
	req, err := http.NewRequest("POST", "/create/players", bytes.NewBuffer(jsonStr))
	if err != nil {
		t.Fatal(err)
	}
	req.Header.Set("Content-Type", "application/json")
	rr := httptest.NewRecorder()
	handler := http.HandlerFunc(CreateTeamWithPlayers)
	handler.ServeHTTP(rr, req)
	if status := rr.Code; status != http.StatusOK {
		t.Errorf("handler returned wrong status code: got %v want %v",
			status, http.StatusOK)
	}
	expected := []byte(`{"message":"Success"}`)
	if rr.Body.Bytes() == nil {
		t.Errorf("handler returned unexpected body: got %v want %v",
			rr.Body.String(), expected)
	}
}

func TestGetPlayerList(t *testing.T) {
	var player Player
	player.Id = 1
	player.Name = "Erick Bertrand"
	player.TeamId = 1

	var arrPlayer []Player
	arrPlayer = append(arrPlayer, player)

	input, expected := 1, arrPlayer
	result := GetPlayerList(input)

	if result[0].Id != expected[0].Id || result[0].Name != expected[0].Name || result[0].TeamId != expected[0].TeamId {
		t.Errorf("The result of GetPlayerList(%v): %v, expected %v", input, result[0], expected[0])
	}
}

func TestGetTeam(t *testing.T) {
	var team Team
	team.Id = 2
	team.Name = "Arsenal"
	team.City = "London"
	team.Region = "England"

	input, expected := "Arsenal", team
	result, _ := GetTeam(input)

	if result.Id != expected.Id || result.Name != expected.Name || result.City != expected.City || result.Region != expected.Region {
		t.Errorf("The result of GetPlayerList(%v): %v, expected %v", input, result, expected)
	}
}

func TestErrorGetTeam(t *testing.T) {
	var team Team
	team.Id = 0
	team.Name = ""
	team.City = ""
	team.Region = ""

	input, expected := "Arsena", team
	result, err := GetTeam(input)

	if err == nil {
		t.Errorf("The result of GetPlayerList(%v): %v, expected %v", input, result, expected)
	}
}
