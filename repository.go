package main

import (
	"database/sql"
	"fmt"
	"log"

	_ "github.com/go-sql-driver/mysql"
)

func GetPlayers(teamId int) (*sql.Rows, error) {
	db := ConnectDB()
	defer db.Close()
	query := fmt.Sprintf("select * from players where team_id = %d", teamId)
	rows, err := db.Query(query)
	return rows, err
}

func GetTeamByName(teamName string) (*sql.Rows, error) {
	db := ConnectDB()
	defer db.Close()
	query := fmt.Sprintf("select * from teams where name = %q limit 1", teamName)
	rows, err := db.Query(query)
	return rows, err
}

func InsertTeamWithPlayers(request CreateTeamWithPlayerRequest) error {
	db := ConnectDB()
	defer db.Close()
	var team Team
	var errorInsert error
	teamName := request.Team.Name
	teamCity := request.Team.City
	teamRegion := request.Team.Region
	playersName := request.PlayerNames

	insertTeamQuery := fmt.Sprintf("insert into teams (name, city, region) values (%q,%q,%q)", teamName, teamCity, teamRegion)
	_, insertTeamErr := db.Query(insertTeamQuery)
	LogError(insertTeamErr)
	errorInsert = insertTeamErr

	getTeamQuery := fmt.Sprintf("select * from teams where id=(select max(id) from teams)")
	teamRow, getTeamErr := db.Query(getTeamQuery)
	LogError(getTeamErr)
	errorInsert = getTeamErr

	for teamRow.Next() {
		if err := teamRow.Scan(&team.Id, &team.Name, &team.City, &team.Region); err != nil {
			log.Fatal(err.Error())
		}
	}

	for x := range playersName {
		insertPlayerQuery := fmt.Sprintf("insert into players (name, team_id) values (%q,%d)", playersName[x], team.Id)
		_, insertPlayerErr := db.Query(insertPlayerQuery)
		if insertPlayerErr != nil {
			log.Fatal(insertPlayerErr)
			errorInsert = insertPlayerErr
		}
	}
	return errorInsert
}

func LogError(err error) {
	if err != nil {
		log.Fatal(err)
	}
}
