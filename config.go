package main

import (
	"database/sql"
	"fmt"
	"log"
)

func ConnectDB() *sql.DB {
	userName := "root"
	password := ""
	host := "localhost"
	dbName := "soccer"
	dataSource := fmt.Sprintf("%s:%s@tcp(%s:3306)/%s", userName, password, host, dbName)
	db, err := sql.Open("mysql", dataSource)

	if err != nil {
		log.Fatal(err)
	}

	return db
}
