package main

import (
	"errors"
	"log"
)

func GetPlayerList(TeamId int) []Player {
	var player Player
	var arrPlayer []Player

	rows, err := GetPlayers(TeamId)
	LogError(err)

	for rows.Next() {
		if err := rows.Scan(&player.Id, &player.Name, &player.TeamId); err != nil {
			log.Fatal(err.Error())
		} else {
			arrPlayer = append(arrPlayer, player)
		}
	}
	return arrPlayer
}

func GetTeam(TeamName string) (Team, error) {
	var team Team
	rows, err := GetTeamByName(TeamName)
	LogError(err)

	for rows.Next() {
		if err := rows.Scan(&team.Id, &team.Name, &team.City, &team.Region); err != nil {
			log.Fatal(err.Error())
		}
	}

	if team.Id == 0 {
		return team, errors.New("Team not found.")
	}
	return team, nil
}
