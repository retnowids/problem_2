**How to run the application:**

1. Clone this repository (make sure you have installed Go on your computer)
2. Create MySql database with name `soccer`
3. Create the tables and insert the data with queries below

```
CREATE TABLE IF NOT EXISTS teams (
    id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL,
    city varchar(100) NOT NULL,
    region varchar(100) NOT NULL
    );
```

```
    CREATE TABLE IF NOT EXISTS players (
    id int(11) NOT NULL AUTO_INCREMENT PRIMARY KEY,
    name varchar(100) NOT NULL,
    team_id int(11) NOT NULL,
    FOREIGN KEY (team_id) REFERENCES teams(id)
    );
```

```
    INSERT INTO teams (name, city, region) VALUES
    ('Manchester United', 'Manchester', 'England'),
    ('Arsenal', 'London', 'England');
```

```
    INSERT INTO players (name, team_id) VALUES
    ('Erick Bertrand', 1),
    ('Bruno Miguel', 1),
    ('Edinson Cavani', 1),
    ('David de Gea', 1),
    ('Lee Grant', 1),
    ('Masson Greenwood', 1),
    ('Dean Henderson', 1),
    ('Phil Jones', 1),
    ('Juan Mata', 1),
    ('Nemanja Matic', 1),
    ('Paul Pogba', 1),
    ('Bernd Leno', 2),
    ('Kieran Tierney', 2),
    ('William Saliba', 2),
    ('Bukayo Saka', 2),
    ('Dani Ceballos', 2),
    ('Mesut Özil', 2),
    ('Willian', 2),
    ('Rob Holding', 2),
    ('Thomas Partey', 2),
    ('Shkodran Mustafi', 2),
    ('Calum Chambers', 2);
```

4. Adjust your database connection such as `username`, `password`, `host`, and `database name` in `config.go` file. And don't forget to turn on your database connection.

5. There are number of dependencies which need to be imported before running the application. Please get the dependenices through the following commands

```
    go get "github.com/go-sql-driver/mysql"
    go get "github.com/gorilla/mux"
```


6. To run the application, please use the following command -

    go run .


**Endpoints Description**


- Get Team With Players
```
    URL - http://localhost:8080/soccers/players/teams/{team_name}
    Method - GET
```


- Create Team With Players
```
    URL - http://localhost:8080/create/players
    Method - POST
    Body - (content-type = application/json)
    {
	    "team":{
		    "name": "Barcelona",
		    "city": "Barcelona",
		    "region": "Spain"
	    },
	    "player_names": [
		    "Marc-André ter Stegen",
		    "Nélson Semedo",
		    "Gerard Piqué",
		    "Sergio Busquets",
		    "Jean-Clair Todibo"
		    ]
    }
```


7. To run all the unit test cases, please do the following

    `go run .`

    `go test -v`

Hope everything works properly. Thank you.