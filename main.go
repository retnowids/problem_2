package main

import (
	"encoding/json"
	"log"
	"net/http"

	_ "github.com/go-sql-driver/mysql"
	"github.com/gorilla/mux"
)

func main() {
	router := mux.NewRouter()
	router.HandleFunc("/soccers/players/teams/{team_name}", GetTeamWithPlayers).Methods("GET")
	router.HandleFunc("/create/players", CreateTeamWithPlayers).Methods("POST")
	log.Fatal(http.ListenAndServe(":8080", router))
}

func CreateTeamWithPlayers(w http.ResponseWriter, r *http.Request) {
	var response CreateResponse
	var createTeamWithPlayerRequest CreateTeamWithPlayerRequest

	requestErr := json.NewDecoder(r.Body).Decode(&createTeamWithPlayerRequest)
	if requestErr != nil {
		http.Error(w, requestErr.Error(), http.StatusBadRequest)
		return
	}

	insertErr := InsertTeamWithPlayers(createTeamWithPlayerRequest)
	if insertErr != nil {
		http.Error(w, insertErr.Error(), http.StatusBadRequest)
		return
	}

	response.Message = "Success"

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}

func GetTeamWithPlayers(w http.ResponseWriter, r *http.Request) {
	var response Response
	var teamWithPlayers TeamWithPlayers

	vars := mux.Vars(r)
	teamName := vars["team_name"]

	team, err := GetTeam(teamName)
	if err != nil {
		http.Error(w, err.Error(), http.StatusBadRequest)
		return
	}

	arrPlayer := GetPlayerList(team.Id)

	teamWithPlayers.Team = team
	teamWithPlayers.Players = arrPlayer

	response.Message = "Success"
	response.Data = teamWithPlayers

	w.Header().Set("Content-Type", "application/json")
	json.NewEncoder(w).Encode(response)

}
