package main

type CreateTeamRequest struct {
	Name   string `json:"name"`
	City   string `json:"city"`
	Region string `json:"region"`
}

type CreateTeamWithPlayerRequest struct {
	Team        CreateTeamRequest `json:"team"`
	PlayerNames []string          `json:"player_names"`
}
