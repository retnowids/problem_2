package main

type Team struct {
	Id     int    `form:"id" json:"id"`
	Name   string `form:"name" json:"name"`
	City   string `form:"city" json:"city"`
	Region string `form:"region" json:"region"`
}

type Player struct {
	Id     int    `form:"id" json:"id"`
	Name   string `form:"name" json:"name"`
	TeamId int    `form:"team_id" json:"team_id"`
}

type TeamWithPlayers struct {
	Team    Team     `json:"team"`
	Players []Player `json:"players"`
}

type Response struct {
	Message string          `json:"message"`
	Data    TeamWithPlayers `json:"data"`
}

type CreateResponse struct {
	Message string `json:"message"`
}
